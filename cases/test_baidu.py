import os
import time

import allure
import pytest

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

from loguru import logger


@allure.feature('百度搜索')
class TestBaidu:

    def setup_class(self):
        # 获取环境变量headless, 控制是否采用无界面形式运行自动化测试
        headless = os.getenv('headless')

        options = Options()
        # 不显示自动化软件控制提示
        options.add_experimental_option('excludeSwitches', ['enable-automation'])
        if headless is not None and headless.lower() == 'true':
            logger.info('使用无界面方式运行')
            options.add_argument('--headless')  # 无头化
            options.add_argument('--disable-gpu')  # 禁用gpu
            options.add_argument('--no-sandbox')

        service = Service(ChromeDriverManager().install())
        logger.info(service)
        self.driver = webdriver.Chrome(service=service, options=options)
        self.driver.implicitly_wait(5)

    def teardown_class(self):
        self.driver.quit()

    @pytest.mark.parametrize('search_keyword, case_name',
                             [('今日头条', '搜索今日头条'), ('王者荣耀', '搜索王者荣耀')],
                             ids=['搜索今日头条', '搜索王者荣耀'])
    def test_baidu(self, search_keyword, case_name):
        allure.dynamic.title(case_name)
        with allure.step(msg := '打开浏览器，访问 www.baidu.com'):
            logger.info(msg)
            self.driver.get("https://www.baidu.com")

        with allure.step(msg := f'搜索关键词: {search_keyword}'):
            logger.info(msg)
            elem = self.driver.find_element(By.ID, 'kw')
            elem.send_keys(search_keyword, Keys.ENTER)
        time.sleep(2)
        assert search_keyword in self.driver.title
